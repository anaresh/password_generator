defmodule PasswordGeneratorWeb.PageController do
  use PasswordGeneratorWeb, :controller

  def home(conn, _params) do
    # The home page is often custom made,
    # so skip the default app layout.
    password = ""
    render(conn, :home, password: password)
  end
  def generate(conn, params) do
    pass_options = Map.get(params, "password", %{})
    {:ok, password} = pass_options |> PassGeneratorApp.generate()
    render(conn, "home.html", password: password)
  end
end
